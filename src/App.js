import "./App.css";
import NavBar from "./componenets/navbar/navbar";
import ShoppintCart from "./componenets/ShoppingCart/shoppingCart";
import Home from "./componenets/HomePage/home";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";

import {
  BrowserRouter,
  Routes,
  Route,
  Redirect,
  useNavigate,
  Link,
} from "react-router-dom";
import Checkout from "./componenets/ShoppingCart/checkOut";
import OrderDetail from "./componenets/ShoppingCart/orderDetail";

function App() {
  const settings = {
    dots: true,
    fade: true,
    infinite: true,
    speed: 500,
    slideToShow: 1,
    arrows: true,
    slidesToScroll: 1,
    className: "slides",
  };
  return (
    <div className="App">
      <header className=" fixed top-0 left-0 right-0 z-20">
        <NavBar />
      </header>

      <main>
        <BrowserRouter>
          <div>
            <Routes>
              <Route path="/" exact element={<Home />} />
              <Route path="/shopping_cart" exact element={<ShoppintCart />} />
              <Route path="/check_out" exact element={<Checkout />} />
              <Route path="/order_detail" exact element={<OrderDetail />} />
            </Routes>
          </div>
        </BrowserRouter>
      </main>
    </div>
  );
}

export default App;
