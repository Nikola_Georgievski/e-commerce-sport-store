import { Fragment, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCoffee } from "@fortawesome/free-solid-svg-icons";
import { faFacebook } from "@fortawesome/free-brands-svg-icons";
import { faInstagram } from "@fortawesome/free-brands-svg-icons";
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faCheckSquare ,faCoffee } from '@fortawesome/free-solid-svg-icons'
// import { library } from `@fortawesome/fontawesome-svg-core`
// import { fab } from `@fortawesome/free-brand-svg-icons`

// library.add(fab, faCheckSquare, faCoffee)

const footerNavigation = {
  info: [
    { name: "За нас", href: "#" },
    { name: "Вработување", href: "#" },
    { name: "Соработка со нас", href: "#" },
    { name: "Синдикална продажба", href: "#" },
    { name: "Подарок картичка", href: "#" },
    { name: "Продавници", href: "#" },
    { name: "Контакт", href: "#" },
  ],
  help: [
    { name: "Услови на користење", href: "#" },
    { name: "Политика на приватност", href: "#" },
    { name: "Политиката за колачиња", href: "#" },
    { name: "Политика за директен маркетинг", href: "#" },
    { name: "Упатство за регистрација", href: "#" },
    { name: "Како да купите", href: "#" },
    { name: "Одреди големина - Обувки", href: "#" },
    { name: "Одреди големина - Облека", href: "#" },
    { name: "Испорака", href: "#" },
    { name: "Замена на производи", href: "#" },
    { name: "Право на откажување/враќање на средства", href: "#" },
    { name: "Рекламациja", href: "#" },
    { name: "Најчести прашања", href: "#" },
  ],
};

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Footer() {
  const [open, setOpen] = useState(false);

  return (
    <footer aria-labelledby="footer-heading" className="bg-teal-700">
      <h2 id="footer-heading" className="sr-only">
        Footer
      </h2>
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="py-20">
          <div className="grid grid-cols-1 md:grid-cols-12 md:grid-flow-col md:gap-x-8 md:gap-y-16 md:auto-rows-min">
            {/* Image section */}
            <div className="col-span-1 md:col-span-2 lg:row-start-1 lg:col-start-1"></div>

            {/* Sitemap sections */}
            <div className="mt-10 col-span-6 grid grid-cols-2 gap-8 sm:grid-cols-3 md:mt-0 md:row-start-1 md:col-start-3 md:col-span-8 lg:col-start-2 lg:col-span-6">
              <div className="grid grid-cols-1 gap-y-12 sm:col-span-2 sm:grid-cols-2 sm:gap-x-8">
                <div>
                  <h3 className="text-sm font-medium text-white text-left">
                    ПОМОШ ПРИ КУПУВАЊЕ
                  </h3>
                  <ul role="list" className="mt-6 space-y-6">
                    {footerNavigation.help.map((item) => (
                      <li key={item.name} className="text-sm text-left">
                        <a
                          href={item.href}
                          className="text-white hover:text-teal-300"
                        >
                          {item.name}
                        </a>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
              <div>
                <h3 className="text-sm font-medium text-white text-left">
                  ИНФРОМАЦИИ
                </h3>

                <ul role="list" className="mt-6 space-y-6">
                  {footerNavigation.info.map((item) => (
                    <li key={item.name} className="text-sm text-left">
                      <a
                        href={item.href}
                        className="text-white hover:text-teal-300"
                      >
                        {item.name}
                      </a>
                    </li>
                  ))}
                </ul>
              </div>
            </div>

            {/* Newsletter section */}
            <div className="mt-12 md:mt-0 md:row-start-2 md:col-start-3 md:col-span-8 lg:row-start-1 lg:col-start-9 lg:col-span-4">
              <h3 className="text-sm font-medium text-white">
                Sign up for our newsletter
              </h3>
              <p className="mt-6 text-sm text-white">
                The latest deals and savings, sent to your inbox weekly.
              </p>
              <form className="mt-2 flex sm:max-w-md">
                <label htmlFor="email-address" className="sr-only">
                  Email address
                </label>
                <input
                  id="email-address"
                  type="text"
                  autoComplete="email"
                  required
                  className="appearance-none min-w-0 w-full bg-white border border-gray-300 rounded-md shadow-sm py-2 px-4 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:border-indigo-500 focus:ring-1 focus:ring-indigo-500"
                />
                <div className="ml-4 flex-shrink-0 border-2 border-white rounded-lg">
                  <button
                    type="submit"
                    className="w-full bg-teal-700 border-2 border-white border-transparent rounded-md shadow-sm py-2 px-4 flex items-center justify-center text-base font-medium text-white hover:bg-teal-300 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  >
                    Sign up
                  </button>
                </div>
              </form>
              <div className="flex w-25 p-5 rounded-3xl">
                <div className=" lg:mr-32 sm:mr-44">
                  <a href="#">
                    <FontAwesomeIcon
                      icon={faFacebook}
                      className="text-white"
                      size="3x"
                    />
                  </a>
                </div>

                <div>
                  <a href="#">
                    <FontAwesomeIcon
                      icon={faInstagram}
                      className="text-white"
                      size="3x"
                    />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="border-t border-gray-100 py-10 text-white max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 ">
        <div className=" grid grid-cols-2 px-5 text-left ">
          <div>
            <p className="text-sm">
              Не е дозволено превземање или користење на содржината од интернет
              страните на Vision, делумно или целосно a се однесува на логоа,
              трговски марки, комерцијални содржини, ниту истите да се
              отстапуваат на трети лица, јавно да се објавуваат или да се
              користат за било какви цели, без писмена согласност од Спорт
              дооел.
            </p>
          </div>
          <div>
            <p className=" ml-5 text-sm">
              Настојуваме да бидеме што попрецизни во описот на производот,
              фотографијата и самата цена, но не можеме да гарантираме дака сите
              информации се комплетни и без грешка. Сите прикажани производи на
              сајтот се дел од нашата понуда, но не се подразбира дека мораат да
              се достапни во секој момент. Достапноста на производите може да ја
              проверите и на телефонскиот број 02 3055 222.
            </p>
          </div>
        </div>
      </div>
      <div className=" border-t boreder-gray-100  max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <p className="text-sm  text-center text-white py-5">
          &copy; 2021 Workflow, Inc. All rights reserved.
        </p>
      </div>
    </footer>
  );
}
