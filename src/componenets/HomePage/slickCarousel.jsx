import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";

const images = [
  {
    imgHref: "#",
    imgSrc:
      "https://images.unsplash.com/photo-1506501139174-099022df5260?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1351&q=80",
  },
  {
    imgHref: "#",
    imgSrc:
      "https://images.unsplash.com/photo-1523438097201-512ae7d59c44?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80",
  },
  {
    imgHref: "#",
    imgSrc:
      "https://images.unsplash.com/photo-1513026705753-bc3fffca8bf4?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80",
  },
];
const settings = {
  dots: true,
  infinite: true,
  slidesToShow: 1,
  autoplay: true,
  autoplaySpeed: 1500,
  pauseOnHover: true,
  arrows: false,
};
export default function SlickCarousel() {
  return (
    <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 h-full my-40">
      <Slider {...settings}>
        {images.map((img, indx) => (
          <div key={indx} className="h-96">
            <a href={img.imgHref}>
              <img
                src={img.imgSrc}
                className=" object-center object-cover h-full mx-auto w-full hover:opacity-75"
              />
            </a>
          </div>
        ))}
      </Slider>
    </div>
  );
}
