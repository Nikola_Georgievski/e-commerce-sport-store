import React, { Component } from "react";

const categories = [
  {
    name: "Nike",
    href: "#",
    imageSrc:
      "https://www.seekpng.com/png/full/391-3910645_nike-logo-transparent-white-pictures-png-nike-png.png",
  },
  {
    name: "Adidas",
    href: "#",
    imageSrc: "https://www.pngarts.com/files/8/Adidas-Logo-PNG-Image.png",
  },
  {
    name: "Converse",
    href: "#",
    imageSrc: "https://logowik.com/content/uploads/images/880_converse.jpg",
  },
  {
    name: "Reebok",
    href: "#",
    imageSrc:
      "https://preview.thenewsmarket.com/Previews/RBOK/StillAssets/1920x1080/551064.png",
  },
  {
    name: "Timberland",
    href: "#",
    imageSrc:
      "https://www.nicepng.com/png/full/162-1625327_timberland-logo.png",
  },
  {
    name: "Champion",
    href: "#",
    imageSrc: "https://wallpaperaccess.com/full/1765937.png",
  },
  {
    name: "The North Face",
    href: "#",
    imageSrc:
      "https://www.seekpng.com/png/detail/40-404862_north-face-logo-logo-vector-the-north-face.png",
  },
  {
    name: "Lotto",
    href: "#",
    imageSrc:
      " https://logos-marques.com/wp-content/uploads/2020/07/Loto-logo.png",
  },
];

export default function BrandList() {
  return (
    <div className="bg-white max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
      <div className="py-16 sm:py-24 xl:max-w-7xl xl:mx-auto xl:px-8">
        <div className="px-4 sm:px-6 flex sm:items-center sm:justify-between lg:px-8 xl:px-0">
          <h2 className="text-2xl font-extrabold tracking-tight text-gray-900">
            Brands
          </h2>
          <a
            href="#"
            className="hidden text-sm font-semibold text-teal-700 hover:text-teal-500 sm:block"
          >
            Browse all brands<span aria-hidden="true"> &rarr;</span>
          </a>
        </div>

        <div className="mt-4 flow-root">
          <div className="-my-2">
            <div className="box-content relative h-80 py-5 overflow-x-scroll">
              <div className="absolute min-w-screen-xl px-4 flex space-x-8 sm:px-6">
                {categories.map((category) => (
                  <a
                    key={category.name}
                    href={category.href}
                    className="relative w-56 h-80 rounded-lg p-6 flex flex-col overflow-hidden hover:opacity-75"
                  >
                    <span aria-hidden="true" className="absolute inset-0">
                      <img
                        src={category.imageSrc}
                        alt=""
                        className="w-full h-full object-center object-contain"
                      />
                    </span>
                    <span
                      aria-hidden="true"
                      className="absolute inset-x-0 bottom-0 h-2/3 bg-gradient-to-t from-black opacity-30"
                    />
                    <span className="relative mt-auto text-center text-xl font-bold text-white">
                      {category.name}
                    </span>
                  </a>
                ))}
              </div>
            </div>
          </div>
        </div>

        <div className="mt-6 px-4 sm:hidden">
          <a
            href="#"
            className="block text-sm font-semibold text-indigo-600 hover:text-indigo-500"
          >
            Browse all categories<span aria-hidden="true"> &rarr;</span>
          </a>
        </div>
      </div>
    </div>
  );
}
