import React, { Component } from "react";
// import ItemTerm from "./itemTerm";

// export default function ItemList() {
//   return (
//     <div class="grid lg:grid-cols-6 sm:grid-cols-2 lg:gap-6 text-white lg:h-32 mt-5 ">

//     {/* <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8"> */}

//       <ItemTerm/>

//       <ItemTerm/>

//       <ItemTerm/>

//       <ItemTerm/>

//       <ItemTerm/>

//       <ItemTerm/>

//     </div>
//   );
// }
const people = [
  {
    name: "Lindsay Walton",
    role: "Front-end Developer",
    imageUrl:
      "https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/d4f5ee75-ea20-461d-a0b4-3aa0124bf5ae/air-huarache-shoes-DmrlKB.png",
    twitterUrl: "#",
    linkedinUrl: "#",
  },
  {
    name: "Lindsay Walton",
    role: "Front-end Developer",
    imageUrl:
      "https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/d4f5ee75-ea20-461d-a0b4-3aa0124bf5ae/air-huarache-shoes-DmrlKB.png",
    twitterUrl: "#",
    linkedinUrl: "#",
  },
  {
    name: "Lindsay Walton",
    role: "Front-end Developer",
    imageUrl:
      "https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/d4f5ee75-ea20-461d-a0b4-3aa0124bf5ae/air-huarache-shoes-DmrlKB.png",
    twitterUrl: "#",
    linkedinUrl: "#",
  },
  {
    name: "Lindsay Walton",
    role: "Front-end Developer",
    imageUrl:
      "https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/d4f5ee75-ea20-461d-a0b4-3aa0124bf5ae/air-huarache-shoes-DmrlKB.png",
    twitterUrl: "#",
    linkedinUrl: "#",
  },
  {
    name: "Lindsay Walton",
    role: "Front-end Developer",
    imageUrl:
      "https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/d4f5ee75-ea20-461d-a0b4-3aa0124bf5ae/air-huarache-shoes-DmrlKB.png",
    twitterUrl: "#",
    linkedinUrl: "#",
  },
  {
    name: "Lindsay Walton",
    role: "Front-end Developer",
    imageUrl:
      "https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/d4f5ee75-ea20-461d-a0b4-3aa0124bf5ae/air-huarache-shoes-DmrlKB.png",
    twitterUrl: "#",
    linkedinUrl: "#",
  },
];

export default function Discounts() {
  return (
    <div className=" bg-gray-100  text-gray-800 py-10">
      <div className="mx-auto py-12 px-4 max-w-7xl sm:px-6 lg:px-8 lg:py-5 ">
        <div className="space-y-12">
          <div class="px-4 sm:px-6 sm:flex sm:items-center sm:justify-between lg:px-8 xl:px-0">
            <h2 class="text-2xl font-extrabold tracking-tight text-gray-900">
              Акција
            </h2>
            <a
              href="#"
              class="hidden text-sm font-semibold text-teal-700 hover:text-teal-500 sm:block"
            >
              Browse all discounts<span aria-hidden="true"> →</span>
            </a>
          </div>

          <ul
            role="list"
            className="space-y-12 sm:grid sm:grid-cols-3 sm:gap-x-6 sm:gap-y-12 sm:space-y-0 lg:grid-cols-6 lg:gap-x-8"
          >
            {people.map((person) => (
              <li key={person.name}>
                <div className="space-y-4">
                  <div className="aspect-w-3 aspect-h-2">
                    <a href="#">
                      <img
                        className="object-cover shadow-lg rounded-lg"
                        src={person.imageUrl}
                        alt=""
                      />
                    </a>
                  </div>

                  <div className="space-y-2">
                    <div className="text-lg leading-6 font-medium space-y-1">
                      <h3>Име на прозивод</h3>

                      <p className=" text-right">Категорија</p>
                      <hr />
                      <p className="text-teal-500 text-right ">**** Ден</p>
                    </div>
                    <ul role="list" className="flex space-x-5"></ul>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}
