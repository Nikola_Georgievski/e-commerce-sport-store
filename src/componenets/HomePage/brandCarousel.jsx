import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";

export default function BrandCarousel() {
  const brands = [
    {
      name: "Nike",
      href: "#",
      imageSrc:
        "https://www.seekpng.com/png/full/391-3910645_nike-logo-transparent-white-pictures-png-nike-png.png",
    },
    {
      name: "Adidas",
      href: "#",
      imageSrc:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Original_Adidas_logo.svg/512px-Original_Adidas_logo.svg.png",
    },
    {
      name: "Converse",
      href: "#",
      imageSrc: "https://logowik.com/content/uploads/images/880_converse.jpg",
    },
    {
      name: "Reebok",
      href: "#",
      imageSrc:
        "https://preview.thenewsmarket.com/Previews/RBOK/StillAssets/1920x1080/551064.png",
    },
    {
      name: "Timberland",
      href: "#",
      imageSrc:
        "https://www.nicepng.com/png/full/162-1625327_timberland-logo.png",
    },
    {
      name: "Champion",
      href: "#",
      imageSrc: "https://wallpaperaccess.com/full/1765937.png",
    },
    {
      name: "The North Face",
      href: "#",
      imageSrc:
        "https://scontent.fskp2-1.fna.fbcdn.net/v/t1.6435-9/128145154_3374788785952677_8690156564149753633_n.jpg?_nc_cat=100&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=UTVyxv58ZF0AX8qRzIM&_nc_ht=scontent.fskp2-1.fna&oh=00_AT-tRWuJuBpvxBFIPFKaivkkOwFMdeheU2VKnHtKp0UodA&oe=61F24BED",
    },
    {
      name: "Lotto",
      href: "#",
      imageSrc:
        " https://logos-marques.com/wp-content/uploads/2020/07/Loto-logo.png",
    },
  ];
  const settings = {
    dots: true,
    infinite: true,
    arrows: true,
    speed: 700,
    slidesToShow: 4,
  };
  return (
    <div className="w-3/4 sm:mx-auto mx-auto px-4 sm:px-6 lg:px-8 h-72">
      <h2 class="text-2xl font-extrabold tracking-tight text-gray-900 text-center mb-10">
        Brands
      </h2>
      <Slider {...settings}>
        {brands.map((bra, indx) => (
          <div key={indx}>
            <a href={bra.href}>
              <img
                src={bra.imageSrc}
                className="object-center object-contain h-28 mx-auto"
              />
            </a>
          </div>
        ))}
      </Slider>
    </div>
  );
}
