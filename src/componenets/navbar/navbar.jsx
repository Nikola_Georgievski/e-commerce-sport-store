import { Fragment, useState } from "react";
import { Dialog, Popover, Tab, Transition } from "@headlessui/react";
import {
  MenuIcon,
  SearchIcon,
  ShoppingCartIcon,
  UserIcon,
  XIcon,
} from "@heroicons/react/outline";

const currencies = ["CAD", "USD", "AUD", "EUR", "GBP"];
const navigation = {
  categories: [
    {
      name: "СЕЗОНСКО НАМАЛУВАЊЕ",
      featured: [
        { name: "Обувки", href: "#" },
        { name: "Текстил", href: "#" },
        { name: "Опрема", href: "#" },
      ],
      collection: [
        { name: "Обувки", href: "#" },
        { name: "Текстил", href: "#" },
        { name: "Опрема", href: "#" },
      ],
      categories: [
        { name: "Обувки", href: "#" },
        { name: "Текстил", href: "#" },
        { name: "Опрема", href: "#" },
      ],
      brands: [{ name: "", href: "#" }],
      catNames: ["МАЖИ", "ЖЕНИ", "ДЕЦА", false],
      special: [
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://www.sportvision.mk/files/images/2021/12/15/nike-390x388.webp",
          span2: false,
          imgLink: "#",
        },
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://www.sportvision.mk/files/images/2021/12/15/nike-390x388.webp",
          span2: false,
          imgLink: "#",
        },
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://images.unsplash.com/photo-1513026705753-bc3fffca8bf4?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80",
          span2: true,
          imgLink: "#",
        },
      ],
    },
    {
      name: "МАЖИ",
      featured: [
        { name: "Lifestyle патики", href: "#" },
        { name: "Патики за трчање", href: "#" },
        { name: "Патики за тренинг", href: "#" },
        { name: "Патики за фудбал", href: "#" },
        { name: "Патики за кошарка", href: "#" },
        { name: "Копачки", href: "#" },
        { name: "Кондури и чизми", href: "#" },
        { name: "Nike Air Max", href: "#" },
        { name: "Addidas Terrex", href: "#" },
        { name: "Сите обувки", href: "#" },
      ],
      collection: [
        { name: "Јакна", href: "#" },
        { name: "Елек", href: "#" },
        { name: "Дуксери", href: "#" },
        { name: "Долен дел тренерки", href: "#" },
        { name: "Тренерки", href: "#" },
        { name: "Маици", href: "#" },
        { name: "Маици со долги ракави", href: "#" },
        { name: "Цела Облека", href: "#" },
      ],
      categories: [
        { name: "Ранци", href: "#" },
        { name: "Торби", href: "#" },
        { name: "Топки", href: "#" },
        { name: "Качкет", href: "#" },
        { name: "Тегови", href: "#" },
        { name: "Ракавици", href: "#" },
        { name: "Чорапи", href: "#" },
        { name: "Цела опрема", href: "#" },
      ],
      brands: [
        { name: "Фудбал", href: "#" },
        { name: "Кошарка", href: "#" },
        { name: "Трчање", href: "#" },
        { name: "Скијање", href: "#" },
        { name: "Пливање", href: "#" },
        { name: "Фитнес", href: "#" },
        { name: "LifeStyle", href: "#" },
        { name: "Сите спортови", href: "#" },
      ],
      catNames: ["ОБУВКИ", "ОБЛЕКА", "ОПРЕМА", "СПОРТОВИ"],
      special: [
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://www.sportvision.mk/files/images/2021/12/15/nike-390x388.webp",
          span2: false,
          imgLink: "#",
        },
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://www.sportvision.mk/files/images/2021/12/15/nike-390x388.webp",
          span2: false,
          imgLink: "#",
        },
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://images.unsplash.com/photo-1513026705753-bc3fffca8bf4?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80",
          span2: true,
          imgLink: "#",
        },
      ],
    },
    {
      name: "ЖЕНИ",
      featured: [
        { name: "Lifestyle патики", href: "#" },
        { name: "Патики за трчање", href: "#" },
        { name: "Патики за тренинг", href: "#" },
        { name: "Кондури и чизми", href: "#" },
        { name: "Nike Air Max", href: "#" },
        { name: "Addidas Terrex", href: "#" },
        { name: "Сите обувки", href: "#" },
      ],
      collection: [
        { name: "Јакна", href: "#" },
        { name: "Елек", href: "#" },
        { name: "Дуксери", href: "#" },
        { name: "Долен дел тренерки", href: "#" },
        { name: "Тренерки", href: "#" },
        { name: "Маици со долги ракави", href: "#" },
        { name: "Маици", href: "#" },
        { name: "Хеланки", href: "#" },
        { name: "Цела Облека", href: "#" },
      ],
      categories: [
        { name: "Ранци", href: "#" },
        { name: "Торби", href: "#" },
        { name: "Топка за пилатес", href: "#" },
        { name: "Подлога за тренинг", href: "#" },
        { name: "Јаже за скокање", href: "#" },
        { name: "Тегови", href: "#" },
        { name: "Чорапи", href: "#" },
        { name: "Цела опрема", href: "#" },
      ],
      brands: [
        { name: "Тренинг", href: "#" },
        { name: "Фитнес", href: "#" },
        { name: "Кошарка", href: "#" },
        { name: "Трчање", href: "#" },
        { name: "Скијање", href: "#" },
        { name: "Пливање", href: "#" },
        { name: "LifeStyle", href: "#" },
        { name: "Сите спортови", href: "#" },
      ],
      catNames: ["ОБУВКИ", "ОБЛЕКА", "ОПРЕМА", "СПОРТОВИ"],
      special: [
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://www.sportvision.mk/files/images/2021/12/15/nike-390x388.webp",
          span2: false,
          imgLink: "#",
        },
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://www.sportvision.mk/files/images/2021/12/15/nike-390x388.webp",
          span2: false,
          imgLink: "#",
        },
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://images.unsplash.com/photo-1513026705753-bc3fffca8bf4?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80",
          span2: true,
          imgLink: "#",
        },
      ],
    },
    {
      name: "СПОРТОВИ",
      featured: [
        { name: "Копачки", href: "#" },
        { name: "Adidas Ghosted", href: "#" },
        { name: "Nike Phantom GT", href: "#" },
        { name: "Патики", href: "#" },
        { name: "Дуксери", href: "#" },
        { name: "Тренерки", href: "#" },
        { name: "Маица", href: "#" },
        { name: "Јакни", href: "#" },
        { name: "Торби", href: "#" },
        { name: "Ранец", href: "#" },
        { name: "Топки", href: "#" },
      ],
      collection: [
        { name: "Патики", href: "#" },
        { name: "Nike wmns Essential", href: "#" },
        { name: "Дуксери", href: "#" },
        { name: "Тренерка", href: "#" },
        { name: "Маици", href: "#" },
        { name: "Долен дел тренерки", href: "#" },
        { name: "Шорцеви", href: "#" },
        { name: "Хеланки", href: "#" },
        { name: "Шише за вода", href: "#" },
        { name: "Торби", href: "#" },
      ],
      categories: [
        { name: "Патики", href: "#" },
        { name: "Nike Lebron", href: "#" },
        { name: "Маици", href: "#" },
        { name: "Шорцеви", href: "#" },
        { name: "Чорапи", href: "#" },
        { name: "Топки", href: "#" },
        { name: "Торби", href: "#" },
        { name: "Ранец", href: "#" },
      ],
      brands: [
        { name: "Патики", href: "#" },
        { name: "Adidas Boost", href: "#" },
        { name: "Nike Zoom Pegasus", href: "#" },
        { name: "Дуксер", href: "#" },
        { name: "Маици", href: "#" },
        { name: "Маици со долги ракави", href: "#" },
        { name: "Чорапи", href: "#" },
        { name: "Шорцеви", href: "#" },
        { name: "Хеланки", href: "#" },
        { name: "Ранец", href: "#" },
      ],
      catNames: ["ФУДБАЛ", "ТРЕНИНГ", "КОШАРКА", "ТРЧАЊЕ", "ОСТАНАТИ"],
      special: [
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://www.sportvision.mk/files/images/2021/12/15/nike-390x388.webp",
          span2: false,
          imgLink: "#",
        },
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://www.sportvision.mk/files/images/2021/12/15/nike-390x388.webp",
          span2: false,
          imgLink: "#",
        },
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://images.unsplash.com/photo-1513026705753-bc3fffca8bf4?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80",
          span2: true,
          imgLink: "#",
        },
      ],
    },
    {
      name: "БРЕНДОВИ",
      featured: [
        { name: "Casual", href: "#" },
        { name: "Boxers", href: "#" },
        { name: "Outdoor", href: "#" },
      ],
      collection: [
        { name: "Everything", href: "#" },
        { name: "Core", href: "#" },
        { name: "New Arrivals", href: "#" },
        { name: "Sale", href: "#" },
      ],
      categories: [
        { name: "Artwork Tees", href: "#" },
        { name: "Pants", href: "#" },
        { name: "Accessories", href: "#" },
        { name: "Boxers", href: "#" },
        { name: "Basic Tees", href: "#" },
      ],
      brands: [{ name: "", href: "#" }],
      catNames: ["МАЖИ", "ЖЕНИ", "ДЕЦА", false],
      special: [
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://www.sportvision.mk/files/images/2021/12/15/nike-390x388.webp",
          span2: false,
          imgLink: "#",
        },
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://www.sportvision.mk/files/images/2021/12/15/nike-390x388.webp",
          span2: false,
          imgLink: "#",
        },
        {
          title: "Action",
          text: "Get new...",
          imgSrc:
            "https://images.unsplash.com/photo-1513026705753-bc3fffca8bf4?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80",
          span2: true,
          imgLink: "#",
        },
      ],
    },
  ],
  pages: [{ name: "НОВОСТИ", href: "#" }],
};

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function NavBar() {
  const [open, setOpen] = useState(false);

  return (
    <div className="bg-white w-a ">
      {/* Mobile menu */}
      <Transition.Root show={open} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 flex  lg:hidden"
          onClose={setOpen}
        >
          <Transition.Child
            as={Fragment}
            enter="transition-opacity ease-linear duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition-opacity ease-linear duration-300"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <Transition.Child
            as={Fragment}
            enter="transition ease-in-out duration-300 transform"
            enterFrom="-translate-x-full"
            enterTo="translate-x-0"
            leave="transition ease-in-out duration-300 transform"
            leaveFrom="translate-x-0"
            leaveTo="-translate-x-full"
          >
            <div className="relative max-w-xs w-full bg-white shadow-xl pb-12 flex flex-col mt-16 overflow-y-scroll">
              <div className="px-4 pt-5 pb-2 flex">
                <button
                  type="button"
                  className="-m-2 p-2 rounded-md inline-flex items-center justify-center text-gray-400"
                  onClick={() => setOpen(false)}
                >
                  <span className="sr-only">Close menu</span>
                  <XIcon className="h-6 w-6" aria-hidden="true" />
                </button>
              </div>

              {/* Links */}
              <Tab.Group as="div" className="mt-2">
                <div className="border-b border-gray-200">
                  <Tab.List className="-mb-px grid px-4 space-x-0 overflow-y-scroll text-left bg-teal-500 pb-5">
                    {navigation.categories.map((category) => (
                      <Tab
                        key={category.name}
                        className={({ selected }) =>
                          classNames(
                            selected
                              ? "text-white border-white"
                              : " text-white border-transparent opacity-50",
                            "flex-1 whitespace-nowrap py-5 px-1 border-b-2 text-base font-bold text-left"
                          )
                        }
                      >
                        {category.name}
                      </Tab>
                    ))}
                  </Tab.List>
                </div>
                <Tab.Panels as={Fragment}>
                  {navigation.categories.map((category, categoryIdx) => (
                    <Tab.Panel
                      key={category.name}
                      className="px-4 pt-10 pb-6 space-y-12"
                    >
                      <div className="grid grid-cols-1 items-start gap-y-10 gap-x-6">
                        <div className="grid grid-cols-1 gap-y-10 gap-x-6">
                          <div>
                            <p
                              id={`mobile-featured-heading-${categoryIdx}`}
                              className={`font-medium text-teal-600 pb-5 text-left ${
                                category.catNames[0]
                                  ? "border-b-2 border-b-teal-500"
                                  : ""
                              }`}
                            >
                              {category.catNames[0]}
                            </p>
                            <ul
                              role="list"
                              aria-labelledby={`mobile-featured-heading-${categoryIdx}`}
                              className="mt-6 space-y-6"
                            >
                              {category.featured.map((item) => (
                                <li key={item.name} className="flex">
                                  <a href={item.href} className="text-gray-500">
                                    {item.name}
                                  </a>
                                </li>
                              ))}
                            </ul>
                          </div>
                          <div>
                            <p
                              id="mobile-categories-heading"
                              className={`font-medium text-teal-600 pb-5 text-left ${
                                category.catNames[1]
                                  ? "border-b-2 border-b-teal-500"
                                  : ""
                              }`}
                            >
                              {category.catNames[1]}
                            </p>
                            <ul
                              role="list"
                              aria-labelledby="mobile-categories-heading"
                              className="mt-6 space-y-6"
                            >
                              {category.categories.map((item) => (
                                <li key={item.name} className="flex">
                                  <a href={item.href} className="text-gray-500">
                                    {item.name}
                                  </a>
                                </li>
                              ))}
                            </ul>
                          </div>
                        </div>
                        <div className="grid grid-cols-1 gap-y-10 gap-x-6">
                          <div>
                            <p
                              id="mobile-collection-heading"
                              className={`font-medium text-teal-600 pb-5 text-left ${
                                category.catNames[2]
                                  ? "border-b-2 border-b-teal-500"
                                  : ""
                              }`}
                            >
                              {category.catNames[2]}
                            </p>
                            <ul
                              role="list"
                              aria-labelledby="mobile-collection-heading"
                              className="mt-6 space-y-6"
                            >
                              {category.collection.map((item) => (
                                <li key={item.name} className="flex">
                                  <a href={item.href} className="text-gray-500">
                                    {item.name}
                                  </a>
                                </li>
                              ))}
                            </ul>
                          </div>
                          <div>
                            <p
                              id="mobile-brand-heading"
                              className={`font-medium text-teal-600 pb-5 text-left ${
                                category.catNames[3]
                                  ? "border-b-2 border-b-teal-500"
                                  : ""
                              }`}
                            >
                              {category.catNames[3]}
                            </p>
                            <ul
                              role="list"
                              aria-labelledby="mobile-brand-heading"
                              className="mt-6 space-y-6"
                            >
                              {category.brands.map((item) => (
                                <li key={item.name} className="flex">
                                  <a href={item.href} className="text-gray-500">
                                    {item.name}
                                  </a>
                                </li>
                              ))}
                            </ul>
                          </div>
                        </div>
                      </div>
                    </Tab.Panel>
                  ))}
                </Tab.Panels>
              </Tab.Group>

              <div className="border-t border-gray-200 py-6 px-4 space-y-6">
                {navigation.pages.map((page) => (
                  <div key={page.name} className="flow-root">
                    <a
                      href={page.href}
                      className="-m-2 p-2 block font-medium text-teal-600"
                    >
                      {page.name}
                    </a>
                  </div>
                ))}
              </div>

              <div className="border-t border-gray-200 py-6 px-4 space-y-6 text-white bg-teal-500">
                <div className="flow-root">
                  <a href="#" className="-m-2 p-2 hidden sm:block  ">
                    Create an account
                  </a>
                </div>
                <div className="flow-root">
                  <a href="#" className="-m-2 p-2 hidden sm:block  font-medium ">
                    Sign in
                  </a>
                </div>
              </div>

              <div className="border-t border-gray-200 py-6 px-4 space-y-6">
                {/* Currency selector */}
              </div>
            </div>
          </Transition.Child>
        </Dialog>
      </Transition.Root>

      <header className="relative">
        <nav aria-label="Top">
          {/* Top navigation */}
          <div className=" bg-teal-500 py-3">
            <div className="max-w-7xl mx-auto h-10 px-4 flex items-center justify-between sm:px-6 lg:px-8">
              {/* Currency selector */}

              <div className="flex-1 lg:text-center sm:text-right text-sm font-medium text-white lg:flex-none sm:mr-10 hidden lg:block xl:block">
                <a
                  href="#"
                  className="text-sm font-medium text-white hover:text-teal-200 lg:pr-5 sm:text-left "
                >
                  Create an account
                </a>
                <span
                  className="h-6 w-px sm:px-28 lg:px-0 bg-teal-500"
                  aria-hidden="true"
                />
                <a
                  href="#"
                  className="text-sm font-medium text-white hover:text-teal-200 sm:text-right"
                >
                  Sign in
                </a>
              </div>

              <div className=" lg:flex lg:flex-1 lg:items-center lg:justify-center lg:space-x-6 hidden ">
                <a href="/" className="">
                  <span className="sr-only">Workflow</span>
                  <img
                    src="https://tailwindui.com/img/logos/workflow-mark.svg?color=indigo&shade=600"
                    alt=""
                    className="h-8 w-auto sm:hidden lg:block ml-60"
                  />
                </a>
              </div>

              <div className="hidden lg:flex lg:flex-1 lg:items-center lg:justify-end lg:space-x-6">
                <div className="flex-1 flex items-center justify-end">
                  <div className="flex items-center lg:ml-8">
                    <div className="flex space-x-8">
                      <div className="hidden lg:flex">
                        <a
                          href="#"
                          className="-m-2 p-2 text-white hover:text-teal-200"
                        >
                          <span className="sr-only">Search</span>
                          <SearchIcon className="w-6 h-6" aria-hidden="true" />
                        </a>
                      </div>

                      <div className="flex">
                        <a
                          href="#"
                          className="-m-2 p-2 text-white hover:text-teal-200"
                        >
                          <span className="sr-only">Account</span>
                          <UserIcon className="w-6 h-6" aria-hidden="true" />
                        </a>
                      </div>
                    </div>

                    <span
                      className="mx-4 h-6 w-px bg-white lg:mx-6"
                      aria-hidden="true"
                    />

                    <div className="flow-root">
                      <a
                        href="/shopping_cart"
                        className="group -m-2 p-2 flex items-center"
                      >
                        <ShoppingCartIcon
                          className="flex-shrink-0 h-6 w-6 text-white group-hover:text-teal-200"
                          aria-hidden="true"
                        />
                        <span className="ml-2 text-sm font-medium text-white group-hover:text-teal-200">
                          0
                        </span>
                        <span className="sr-only">items in cart, view bag</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* Secondary navigation */}
          <div className="bg-white">
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
              <div className="border-b border-gray-200">
                <div className="h-16 flex items-center justify-between">
                  {/* Logo (lg+) */}

                  <div className="hidden h-full lg:flex">
                    {/* Mega menus */}
                    <Popover.Group className="">
                      <div className="h-full flex justify-center space-x-8">
                        {navigation.categories.map((category, categoryIdx) => (
                          <Popover key={category.name} className="flex">
                            {({ open }) => (
                              <>
                                <div className="relative flex">
                                  <Popover.Button
                                    className={classNames(
                                      open
                                        ? "border-teal-600 text-teal-600"
                                        : "border-transparent text-gray-700 hover:text-teal-700",
                                      "relative z-0 flex items-center transition-colors ease-out duration-200 text-sm font-medium border-b-2 -mb-px pt-px"
                                    )}
                                  >
                                    {category.name}
                                  </Popover.Button>
                                </div>

                                <Transition
                                  as={Fragment}
                                  enter="transition ease-out duration-200"
                                  enterFrom="opacity-0"
                                  enterTo="opacity-100"
                                  leave="transition ease-in duration-150"
                                  leaveFrom="opacity-100"
                                  leaveTo="opacity-0"
                                >
                                  <Popover.Panel className="absolute top-full inset-x-0 text-gray-500 sm:text-sm">
                                    {/* Presentational element used to render the bottom shadow, if we put the shadow on the actual panel it pokes out the top, so we use this shorter element to hide the top of the shadow */}
                                    <div
                                      className="absolute inset-0 top-1/2 bg-white shadow"
                                      aria-hidden="true"
                                    />

                                    <div className="relative bg-white z-10">
                                      <div className="max-w-7xl mx-auto px-8">
                                        <div className="grid grid-cols-2 items-start gap-y-10 gap-x-8 pt-10 pb-12">
                                          <div className="grid grid-cols-4 gap-y-10 gap-x-8 text-left">
                                            <div>
                                              <p
                                                id={`desktop-featured-heading-${categoryIdx}`}
                                                className={`font-medium text-teal-600 pb-5 text-left ${
                                                  category.catNames[0]
                                                    ? "border-b-2 border-b-teal-500"
                                                    : ""
                                                }`}
                                              >
                                                {category.catNames[0]}
                                              </p>

                                              <ul
                                                role="list"
                                                aria-labelledby={`desktop-featured-heading-${categoryIdx}`}
                                                className="mt-6 space-y-6 sm:mt-4 sm:space-y-4"
                                              >
                                                {category.featured.map(
                                                  (item) => (
                                                    <li
                                                      key={item.name}
                                                      className="flex"
                                                    >
                                                      <a
                                                        href={item.href}
                                                        className="hover:text-teal-700"
                                                      >
                                                        {item.name}
                                                      </a>
                                                    </li>
                                                  )
                                                )}
                                              </ul>
                                            </div>
                                            <div>
                                              <p
                                                id="desktop-categories-heading"
                                                className={`font-medium text-teal-600 pb-5 text-left ${
                                                  category.catNames[1]
                                                    ? "border-b-2 border-b-teal-500"
                                                    : ""
                                                }`}
                                              >
                                                {category.catNames[1]}
                                              </p>
                                              <ul
                                                role="list"
                                                aria-labelledby="desktop-categories-heading"
                                                className="mt-6 space-y-6 sm:mt-4 sm:space-y-4"
                                              >
                                                {category.categories.map(
                                                  (item) => (
                                                    <li
                                                      key={item.name}
                                                      className="flex"
                                                    >
                                                      <a
                                                        href={item.href}
                                                        className="hover:text-teal-700"
                                                      >
                                                        {item.name}
                                                      </a>
                                                    </li>
                                                  )
                                                )}
                                              </ul>
                                            </div>
                                            <div>
                                              <p
                                                id="desktop-collection-heading"
                                                className={`font-medium text-teal-600 pb-5 text-left ${
                                                  category.catNames[2]
                                                    ? "border-b-2 border-b-teal-500"
                                                    : ""
                                                }`}
                                              >
                                                {category.catNames[2]}
                                              </p>
                                              <ul
                                                role="list"
                                                aria-labelledby="desktop-collection-heading"
                                                className="mt-6 space-y-6 sm:mt-4 sm:space-y-4"
                                              >
                                                {category.collection.map(
                                                  (item) => (
                                                    <li
                                                      key={item.name}
                                                      className="flex"
                                                    >
                                                      <a
                                                        href={item.href}
                                                        className="hover:text-teal-700"
                                                      >
                                                        {item.name}
                                                      </a>
                                                    </li>
                                                  )
                                                )}
                                              </ul>
                                            </div>
                                            <div>
                                              <p
                                                id="desktop-brand-heading"
                                                className={`font-medium text-teal-600 pb-5 text-left ${
                                                  category.catNames[3]
                                                    ? "border-b-2 border-b-teal-500"
                                                    : ""
                                                }`}
                                              >
                                                {category.catNames[3]}
                                              </p>
                                              <ul
                                                role="list"
                                                aria-labelledby="desktop-brand-heading"
                                                className="mt-6 space-y-6 sm:mt-4 sm:space-y-4"
                                              >
                                                {category.brands.map((item) => (
                                                  <li
                                                    key={item.name}
                                                    className="flex"
                                                  >
                                                    <a
                                                      href={item.href}
                                                      className="hover:text-teal-700"
                                                    >
                                                      {item.name}
                                                    </a>
                                                  </li>
                                                ))}
                                              </ul>
                                            </div>
                                          </div>

                                          <div className="grid grid-cols-2 gap-y-10 gap-x-8">
                                            {category.special.map((sp) => (
                                              <div
                                                className={
                                                  sp.span2 ? "col-span-2" : ""
                                                }
                                              >
                                                <a
                                                  href={sp.imgLink}
                                                  className="font-medium text-gray-900"
                                                >
                                                  <img
                                                    src={sp.imgSrc}
                                                    className={`object-center object-cover hover:opacity-75 rounded-xl ${
                                                      sp.span2
                                                        ? "h-48 w-full"
                                                        : " "
                                                    }`}
                                                  />
                                                </a>
                                                <div className="flex flex-col justify-end">
                                                  <div
                                                    className={`p-4 bg-white bg-opacity-60 text-sm absolute ${
                                                      sp.span2
                                                        ? "w-full"
                                                        : "w-72"
                                                    }`}
                                                  >
                                                    <h2 className="font-bold text-xl text-black text-left">
                                                      {sp.title}
                                                    </h2>

                                                    <p
                                                      aria-hidden="true"
                                                      className="mt-0.5 text-gray-700 sm:mt-1 text-left"
                                                    >
                                                      {sp.text}
                                                    </p>
                                                  </div>
                                                </div>
                                              </div>
                                            ))}
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </Popover.Panel>
                                </Transition>
                              </>
                            )}
                          </Popover>
                        ))}

                        {navigation.pages.map((page) => (
                          <a
                            key={page.name}
                            href={page.href}
                            className="flex items-center text-sm font-medium text-gray-700 hover:text-teal-700"
                          >
                            {page.name}
                          </a>
                        ))}
                      </div>
                    </Popover.Group>
                  </div>
                  {/* Mobile menu and search (lg-) */}
                  <div className="flex-1 flex items-center lg:hidden">
                    <button
                      type="button"
                      className="-ml-2 bg-white p-2 rounded-md text-gray-400"
                      onClick={() => setOpen(true)}
                    >
                      <span className="sr-only">Open menu</span>
                      <MenuIcon className="h-6 w-6" aria-hidden="true" />
                    </button>

                    {/* Search */}
                    <a
                      href="#"
                      className="ml-2 p-2 text-gray-400 hover:text-teal-500"
                    >
                      <span className="sr-only">Search</span>
                      <SearchIcon className="w-6 h-6" aria-hidden="true" />
                    </a>
                  </div>
                  {/* Logo (lg-) */}
                  <a href="/" className="lg:hidden ">
                    <span className="sr-only">Workflow</span>
                    <img
                      src="https://tailwindui.com/img/logos/workflow-mark.svg?color=indigo&shade=600"
                      alt=""
                      className="h-8 w-auto"
                    />
                  </a>
                  <div className="flex-1 flex items-center justify-end ">
                    <div className="flex items-center lg:ml-8 lg:hidden">
                      <div className="flex space-x-8">
                        <div className="hidden lg:flex">
                          <a
                            href="#"
                            className="-m-2 p-2 text-gray-400 hover:text-teal-500"
                          >
                            <span className="sr-only">Search</span>
                            <SearchIcon
                              className="w-6 h-6"
                              aria-hidden="true"
                            />
                          </a>
                        </div>
                        <a href="/" className="sm:hidden">
                          <span className="sr-only">Workflow</span>
                          <img
                            className="h-8 w-auto flex"
                            src="https://tailwindui.com/img/logos/workflow-mark.svg?color=indigo&shade=600"
                            alt=""
                          />
                        </a>

                        <div className="flex">
                          <a
                            href="#"
                            className="-m-2 p-2 text-gray-400 hover:text-teal-500"
                          >
                            <span className="sr-only">Account</span>
                            <UserIcon className="w-6 h-6" aria-hidden="true" />
                          </a>
                        </div>
                      </div>

                      <span
                        className="mx-4 h-6 w-px bg-teal-500 lg:mx-6"
                        aria-hidden="true"
                      />

                      <div className="flow-root">
                        <a
                          href="/shopping_cart"
                          className="group -m-2 p-2 flex items-center"
                        >
                          <ShoppingCartIcon
                            className="flex-shrink-0 h-6 w-6 text-gray-400 group-hover:text-teal-500"
                            aria-hidden="true"
                          />
                          <span className="ml-2 text-sm font-medium text-gray-700 group-hover:text-teal-700">
                            0
                          </span>
                          <span className="sr-only">
                            items in cart, view bag
                          </span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </header>
    </div>
  );
}
